require m300_gps, master

#------------------------------------------------------------------------------
# iocStats MACROS
epicsEnvSet(IOCNAME, "m300_gps")
epicsEnvSet(LOCATION, "Acc")

#------------------------------------------------------------------------------
# m300_gps MACROS
epicsEnvSet("P", "TRef-D1:Ctrl-GPS-1:")
epicsEnvSet("R", "")
epicsEnvSet("HOST", "172.16.6.39")
epicsEnvSet("MIBDIRS", "+$(m300_gps_DIR)")
epicsEnvSet("MIB", "MBG-SNMP-LTNG-MIB")
epicsEnvSet("COMMUNITY", "esstn")


#------------------------------------------------------------------------------
iocshLoad("$(m300_gps_DIR)/m300_gps.iocsh", "COMMUNITY=$(COMMUNITY), P=$(P), R=$(R), HOST=$(HOST), MIB=$(MIB)")

